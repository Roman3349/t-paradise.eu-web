<?php
$title = $nazev != null ? $nazev . " | " : null;
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?= $title ?>LuckyCraft</title>
        <meta charset="UTF-8">
        <meta name="author" content="Roman Ondráček">
        <meta name="keywords" content="luckycraft, minecraft, server">
        <meta name="description" content="Web minecraftového serveru LuckyCraft.">
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Menu</span><span class="icon-bar"></span>
                        <span class="icon-bar"></span><span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">LuckyCraft</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="/">Domů</a></li>
                        <li><a href="pravidla.php">Pravidla</a></li>
                        <li><a href="recepty.php">Recepty</a></li>
                        <li><a href="dynmap.php">DynMap</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="jumbotron">      
            <div class="container">
                <h1>LuckyCraft</h1>
                <h2>Minecraft server od lidí pro lidi</h2>
            </div>    
        </div> 
        <div class="container">
            <div class="row">
                <div class="col-lg-8">