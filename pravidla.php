<?php
$nazev = 'Pravidla';
require 'includes/header.php';
?>
<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title">Pravidla</h3>
    </div>
    <div class="panel-body">
        <ol>
            <li>Chovejte se slušně</li>
            <li>Nepoužívejte hacky a nezneužívejte bugů</li>
            <li>Nepoužívejte nicky jiných hráčů</li>
            <li>Kácejte celé stromy</li>
            <li>Neničte krajinu. Těžení za účelem získání materiálu v normálním světě je zakázáno. Použijte těžící svět.</li>
            <li>Nespamujte a nedělejte reklamu jiným projektům v herním chatu</li>
            <li>Nekopejte přímé díry do zem</li>
            <li>Nestavte blocky v oblastech, které jsou již zabrány jiným hráčem</li>
            <li>Resky vytvářejte jen na místech, kde skutečně pracujete nebo máte obydlí</li>
            <li>Nerabujte cizí stavby, obvody či truhly</li>
            <li>Nemějte více jak 200 entit na jednom místě</li>
            <li>Nestavte cyklické obvody lagující server</li>
            <li>Nestavte automatické třídičky</li>
            <li>Vyhrazujeme si právo na ničení nevzhledných staveb</li>
            <li>Pokud spácháte přestupek proti těmto pravidlům, je lepší se přiznat</li>
            <li>Respektujte tým serveru</li>
        </ol>
    </div>
</div>
<?php require 'includes/footer.php';
