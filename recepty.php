<?php
$nazev = 'Recepty';
require 'includes/header.php';
?>
<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title">Recepty</h3>
    </div>
    <div class="panel-body">
        <h4>Kroužkové brnění</h4>
        <img src="images/recipes/chainmail/helmet.png" alt="" title="Recept na kroužkovou kapuci">
	<img src="images/recipes/chainmail/chestplate.png" alt="" title="Recept na kroužkovou košili">
	<img src="images/recipes/chainmail/leggins.png" alt="" title="Recept na koužkové kalhoty">
	<img src="images/recipes/chainmail/boots.png" alt="" title="Recept na koužkové boty">
        <h4>Itemy pro koně</h4>
        <img src="images/recipes/horse/armor.gif" alt="" title="Recept na brnění pro koně">
	<img src="images/recipes/horse/saddle.png" alt="" title="Recept na sedlo">
        <h4>Ostatní</h4>
        <img src="images/recipes/other/pice.png" alt="" title="Recept na balený led">
        <img src="images/recipes/other/record.gif" alt="" title="Recept na desky do hrací skříně">
        <img src="images/recipes/horse/leather.png" alt="" title="Recept na kůži">
    </div>
</div>
<?php require 'includes/footer.php';
